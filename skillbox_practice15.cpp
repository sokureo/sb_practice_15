#include <iostream>


void FindOddNumbers(int Limit, bool IsOdd)
{
    for (int i = IsOdd; i < Limit; i += 2)
    {
        std::cout << i << '\n';
    }
}


int main()
{
    int Limit = 10;
    bool IsOdd = true;
    FindOddNumbers(Limit, IsOdd);
}
